```text
matches:            kkl matches
scoresheet:         spielberichtsbögen
loc:                storage
```

```text
Landingpage (M)
    |- Topbar (C)
    |- Footerbar (C)

Create (M)
    |- Topbar (C)
    |- ScoresheetSetup (C)
        |- FormSetupGameday (C)
        |- FormSetupCup (C)
        |- FormSetupCustom (C)
    |- ScoresheetAddPlayers (C)
    |- ScoresheetDrawMatches (C)
    |- Confirm
    |- Footerbar (C)

Enter (M)
    |- Topbar (C)
    |- ScoresheetResults (C)        <-- ??
        |- ScoresheetTitle (W)
        |- FormEnterResults (C)
        |- ScoresheetTrend (W)
        |- ScoresheetGamedata (W)
        |- ScoresheetPlayers (W)
        |- LiveTable (C)
        |- LiveGames (C)
        |- ScoresheetMap (W)
        |- ScoresheetShare (W)
    |- Footerbar (C)

Live (M)
    |- Topbar (C)
    |- Match (W)
    |- Footerbar (C)
    
Detail (M)
    |- Topbar (C)
    |- ScoresheetTitle (W)
    |- ScoresheetDraw (W)
    |- ScoresheetTrend (W)
    |- ScoresheetGamedata (W)
    |- ScoresheetPlayers (W)
    |- LiveTable (C)
    |- LiveGames (C)
    |- ScoresheetMap (W)
    |- ScoresheetShare (W)
    |- Footerbar (C)

Legend:
M = Module, C = Component, W = Widget
```