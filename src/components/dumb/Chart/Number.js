import React from "react";
import LoadingSpinner from "Dumb/Loading/Spinner";

import "./chart-number.scss";

const ChartNumber = ({name, label, value, avarage}) => (
    <article className={`chart-number ${name}`}>
        <div className="label">{label}</div>
        {(value !== null) ? (
            <div className="value -count">{value}</div>
        ) : (
            <div className="value">
                <LoadingSpinner />
            </div>
        )}

        {(avarage) ? (
            <div className="avarage">
                {avarage}
            </div>
        ) : null}
    </article>
);

export default ChartNumber;