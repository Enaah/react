import React from "react";

const ChartDistribution = ({ goals, players }) => {

    /**
     * 
     * @param {*} total 
     * @param {*} value 
     * @returns 
     */
    const getPercent = (total, value) => {
        return value * 100 / total;
    }

    /**
     * 
     * @param {*} a 
     * @param {*} b 
     * @returns 
     */
    const getWinner = (a, b) => {
        return (a > b) ? '-active' : '';
    }

    return (
        <article className="chart-number">
            <div className="label">Verteilung</div>

            <div className="value">

                <div className="chart-distribution">
                    <div className="head">
                        <div className="home">Heim</div>
                        <div className="label"></div>
                        <div className="guest">Gast</div>
                    </div>

                    {(goals) ? (
                        <div className="content">
                            <div className={'home ' + getWinner(goals.home, goals.guest)}>
                                <span style={{ width: getPercent(goals.total, goals.home) + '%' }}>
                                    {goals.home}
                                </span>
                            </div>
                            <div className="label"><strong>Tore</strong></div>
                            <div className={'guest ' + getWinner(goals.guest, goals.home)}>
                                <span style={{ width: getPercent(goals.total, goals.guest) + '%' }}>
                                    {goals.guest}
                                </span>
                            </div>
                        </div>
                    ) : null}

                    {(players) ? (
                        <div className="content">
                            <div className={'home ' + getWinner(players.home, players.guest)}>
                                <span style={{ width: getPercent(players.total, players.home) + '%' }}>
                                    {players.home}
                                </span>
                            </div>
                            <div className="label"><strong>Spieler</strong></div>
                            <div className={'guest ' + getWinner(players.guest, players.home)}>
                                <span style={{ width: getPercent(players.total, players.guest) + '%' }}>
                                    {players.guest}
                                </span>
                            </div>
                        </div>
                    ) : null}
                </div>

            </div>

        </article>
    );
}

export default ChartDistribution;