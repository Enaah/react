import React, {useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {hideNotification} from "Redux/notificationStore";

import "./notification.scss";

export function Notification() {
    const dispatch = useDispatch();
    const type = useSelector((state) => state.notificationStore.type);
    const message = useSelector((state) => state.notificationStore.message);
    const show = useSelector((state) => state.notificationStore.show);

    if (show) {
        setTimeout(() => dispatch(hideNotification()), 2400);
    }

    return (
        <div className={`notification -fix ${(show) ? '-in' : ''} -${type}`}>
            <span>{message}</span>
        </div>
    )

}
