import React from "react";
import {Navigation} from "Dumb/Navigation/Navigation";
import AccountLink from "../Auth/AccountLink";

import "./topbar.scss";
import Brand from "Images/brand_weiss.svg";

export function Topbar() {

    return (
        <div>
            <section className="topbar -fix">
                <div className="container">
                    <div className="brand">
                        <a href="./">
                            <Brand />
                        </a>
                    </div>
                    <div className="-spacer"></div>
                    <AccountLink label="" />
                    <Navigation />
                </div>
            </section>
            <div className="topbar--spacer"></div>
        </div>
    );
}
