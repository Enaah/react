import React from "react";
import {useSelector} from "react-redux";
import {GameType} from "JS/enums/GameType";
import moment from "moment";

import "./gamedata.scss"

export function ScoresheetGamedata() {
    const scoresheet = useSelector((state) => state.scoresheetStore.scoresheet);

    if (!scoresheet.gameType) {
        return null;
    } else {

        let league = (<div></div>);
        if (scoresheet.gameType === GameType.GAMEDAY) {
            league = (<div><span>Liga</span> {scoresheet.matchInfo.leagueName}</div>);
        }

        let gameType = (<div></div>);
        if (scoresheet.gameType === GameType.GAMEDAY) {
            gameType = (<div><span>Spieltag</span> {scoresheet.matchInfo.gamedayNumber}</div>);
        }

        return (
            <div className="scoresheet-gamedata">
                <div className="grid">
                    {league}
                    {gameType}

                    <div>
                        <span>Spielort</span>
                        {(scoresheet.location) ? scoresheet.location.title : 'Privat'}
                    </div>
                    <div>
                        <span>Datum</span>
                        {moment(scoresheet.date.create).format('DD.MM.YYYY')}
                    </div>
                    <div>
                        <span>Match Id</span>
                        {scoresheet.matchid}
                    </div>
                    <div>
                        <span>Start / Update</span>
                        {moment(scoresheet.date.create).format('HH:mm')} Uhr
                        {(scoresheet.date.update) ?
                            (<small> / {moment(scoresheet.date.update).format('HH:mm')} Uhr</small>)
                            : null}
                    </div>
                </div>
            </div>
        );
    }
}
