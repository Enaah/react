import React, {useEffect, useState} from "react";
import Performance from "JS/rest/Performance";

import "./team-details.scss";
import {Circle} from "Dumb/Circle/Circle";

const TeamDetails = ({team, emit}) => {
    const [promise, setPromise] = useState(true);
    const [players, setPlayers] = useState([]);

    const hideOverlay = () => emit(null);

    useEffect(() => {
        setPromise(false);

        if (team) {
            Performance.playersOverall(team._id, team.season, team.gametype)
                .then(playersList => {
                    setPlayers(playersList);
                    setPromise(true);
                });
        }
    }, [team])

    return (
        <div>
            {(promise && team) ? (
                <div className={`overlay--team-details -fixed`}>
                    <div className="background"></div>

                    <div className="container">
                        <article className="content">
                            <button className="btn-close" onClick={hideOverlay}></button>

                            <header>
                                <h1>{team._id}</h1>
                                <p>{team.gametype} - {team.season}</p>
                                <div className="details">
                                    <div><small>Erfasste</small><br /><strong>Berichte</strong><br/>{team.totalScoresheets}</div>
                                    <div><small>Gewonnene</small><br /><strong>Sätze</strong><br/>{team.totalSets}</div>
                                    <div><small>Erzielte</small><br /><strong>Tore</strong><br/>{team.totalGoals}</div>
                                </div>
                            </header>

                            {(players) ? (
                            <div className="player-list">
                                <div className="table -tiles">
                                    <div className="table-head-players">
                                        <div className="index">#</div>
                                        <div className="name">Name</div>
                                        <div className="goals">Tore</div>
                                        <div className="set">Sätze</div>
                                        <div className="circle"></div>
                                    </div>

                                    {players.map((player, index) => (
                                    <div className="table-row-players" key={index}>
                                        <div className="index">{index + 1}.</div>
                                        <div className="name -ellipsis">{ player._id }</div>
                                        <div className="goals">{player.totalGoalsWin} : {player.totalGoalsLoose}</div>
                                        <div className="set">{player.totalSetWin} : {player.totalSetLoose}</div>
                                        <div className="circle">
                                            <Circle value={player.totalSetWin*100/(player.totalSetWin+player.totalSetLoose)} />
                                        </div>
                                    </div>
                                    ))}
                                </div>
                            </div>
                            ) : null}
                        </article>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default TeamDetails;