import React from "react";
import {connect} from "react-redux";
import {buildScoresheetStore, revertScoresheetStore} from "Redux/scoresheetStore";
import {hidePageLoading, showPageLoading} from "Redux/loadingStore";
import find from "lodash.find"

import Scoresheet from "JS/rest/Scoresheet";
import Editor from "JS/rest/Editor";
import Kickerliga from "JS/rest/Kickerliga";

import {GameType} from "JS/enums/GameType";
import {MatchState} from "JS/enums/MatchState";
import FormSetupGameday from "Smart/Form/SetupGameday";
import FormSetupCup from "Smart/Form/SetupCup";
import FormSetupCustom from "Smart/Form/SetupCustom";
import Tracking from "JS/rest/Tracking";

import "./setup-match.scss"

class FormSetup extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            currentGameType: GameType.GAMEDAY,
            privacy: false
        }
    }

    componentDidMount() {
        this.props.showPageLoading('Load locations');

        Kickerliga.locations().then(locations => {
            this.setState({
                isLoaded: true,
                locations
            });

            this.props.hidePageLoading();
        })
    }


    toggleGameType = (type) => {
        this.props.revertScoresheetStore();
        this.setState({currentGameType: type})
    }


    updateLocation = (event) => {
        const locationId = event.target.value;
        let location = {
            title: locationId
        }

        if (locationId !== 'privat') {
            location = find(this.state.locations, (l) => {
                return l.id === parseFloat(locationId);
            });
        }

        this.props.buildScoresheetStore({build: 'location', data: location})
    }


    togglePrivacy = (event) => {
        this.setState({privacy: event.target.checked});
    }


    submitForm = () => {
        const s = this.props.scoresheetStore.scoresheet;
        if (s
            && s.matchid
            && s.team_home.name
            && s.team_guest.name
            && s.team_home.mail
            && s.team_guest.mail
            && s.location
            && this.state.privacy
        ) {
            Editor.set(s.matchid).then(token => {
                Scoresheet.set.match(s).then(scoresheet => {
                    this.props.buildScoresheetStore({build: 'state', data: MatchState.SETUP_PLAYERS});
                    Tracking.click('enter', MatchState.SETUP_PLAYERS);
                })
            });
        } else {
            // TODO: show missing input fields
            console.log('failure! stay here …', s);
        }
    }


    render() {
        const {isLoaded, currentGameType, locations, privacy} = this.state;

        if (!isLoaded) {
            return null
        } else {
            return (
                <div className="create-setup-match">
                    <div className="section-head">
                        <div className="container">
                            <h1>Neue Begegnung</h1>
                            <p>Fülle bitte <strong>alle Plichtfelder</strong> aus. Die Daten sind für die Zuordnung der
                                Begegnung wichtig. Die E-Mail Adressen der jeweiligen Kapitäne werden bei einem
                                regulärem Ligaspiel vom System automatisch ermittelt.</p>
                        </div>
                    </div>

                    <div className="section-form">
                        <div className="container">
                            <div className="row">
                                <div className="-spacer"></div>
                                <div className="form-group">
                                    <div className="input-group">
                                        <input type="radio" id={GameType.GAMEDAY} className="input-radio"
                                               name="gameType"
                                               value={GameType.GAMEDAY}
                                               checked={currentGameType === GameType.GAMEDAY}
                                               onChange={() => this.toggleGameType(GameType.GAMEDAY)}/>
                                        <label htmlFor={GameType.GAMEDAY}>Spieltag</label>
                                    </div>

                                    <div className="input-group">
                                        <input type="radio" id={GameType.CUP} className="input-radio"
                                               name="gameType"
                                               value={GameType.CUP}
                                               checked={currentGameType === GameType.CUP}
                                               onChange={() => this.toggleGameType(GameType.CUP)}/>
                                        <label htmlFor={GameType.CUP}>Pokal</label>
                                    </div>

                                    <div className="input-group">
                                        <input type="radio" id={GameType.CUSTOM} className="input-radio"
                                               name="gameType"
                                               value={GameType.CUSTOM}
                                               checked={currentGameType === GameType.CUSTOM}
                                               onChange={() => this.toggleGameType(GameType.CUSTOM)}/>
                                        <label htmlFor={GameType.CUSTOM}>Freundschaftsspiel</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {(currentGameType === GameType.GAMEDAY) ? (<FormSetupGameday/>) : null}
                        {(currentGameType === GameType.CUP) ? (<FormSetupCup/>) : null}
                        {(currentGameType === GameType.CUSTOM) ? (<FormSetupCustom/>) : null}

                        <div className="container">
                            <div className="row input-group">
                                <label htmlFor="locations">Spielort</label>
                                <select id="locations" className="input-select"
                                        name="locations"
                                        onChange={this.updateLocation}
                                        defaultValue={false}>
                                    <option value="false" disabled={true}>Wähle ein Spielort</option>
                                    <option value="privat">Privat</option>
                                    {locations.map(location => (
                                        <option value={location.id} key={location.id}>
                                            {location.title}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="section-submit -bordered">
                        <div className="container">
                            <div className="-spacer"></div>
                            <div>
                                <div className="input-group">
                                    <input type="checkbox" id="privacy" className="input-checkbox"
                                           name="privacy"
                                           value="true"
                                           onChange={this.togglePrivacy}/>
                                    <label htmlFor="privacy">
                                        Ich stimme zu, dass alle auf dieser Webseite erfassten Daten für die Dauer der
                                        Bearbeitung auf meinem Device zwischengespeichert werden. Die gespeicherten
                                        Daten werden nach Abschluss des Spiels sofort wieder gelöscht.
                                    </label>
                                </div>

                                <button type="submit" className="btn-cta" data-icon="players"
                                        disabled={!(privacy)} onClick={this.submitForm}>
                                    Weiter zu den Teamdetails
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
    revertScoresheetStore,
    buildScoresheetStore
})(FormSetup);
