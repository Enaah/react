import React from "react";
import {connect} from "react-redux";

import {showPageLoading, hidePageLoading} from "Redux/loadingStore";
import {buildScoresheetStore} from "Redux/scoresheetStore";

import Cup from "JS/rest/Cup";
import {GameType} from "JS/enums/GameType";
import find from "lodash.find";

class FormSetupCup extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            team_home: '-',
            team_guest: '-'
        }

        this.chooseCupMatch = this.chooseCupMatch.bind(this);
        this.updateMail = this.updateMail.bind(this);
    }

    componentDidMount() {
        this.props.showPageLoading('load matches');

        this.props.buildScoresheetStore({build: 'id', data: new Date().getTime()});
        this.props.buildScoresheetStore({build: 'match', data: {gameType: GameType.CUP}});

        Cup.matches().then(matches => {
            this.setState({
                isLoaded: true,
                locations: this.props.locations,
                matches
            });

            this.props.hidePageLoading();
        });
    }

    /**
     * render list of matches
     * @returns {JSX.Element}
     */
    renderCupMatches() {
        const {matches} = this.state;

        if (matches) {
            return (
                <div className="row input-group">
                    <label htmlFor="match">Begegnung</label>
                    <select id="match" className="input-select"
                            name="league"
                            onChange={this.chooseCupMatch}
                            disabled={(matches.length === 0) ? 'disabled' : ''}
                            defaultValue="0">
                        <option value="0" disabled>Begegnung wählen</option>
                        {matches.map(m => (
                            <option value={m.match.id}
                                    key={m.match.id}>{m.match.player1_name} vs {m.match.player2_name}</option>
                        ))}
                    </select>
                </div>
            )
        }
    }


    /**
     *
     * @param event
     */
    chooseCupMatch(event) {
        const match = find(this.state.matches, (m) => {
            return m.match.id === parseFloat(event.target.value);
        });

        this.props.buildScoresheetStore({
            build: 'team',
            data: {
                type: 'team_home', team: {
                    id: match.match.player1_id,
                    name: match.match.player1_name
                }
            }
        });
        this.props.buildScoresheetStore({
            build: 'team',
            data: {
                type: 'team_guest', team: {
                    id: match.match.player2_id,
                    name: match.match.player2_name
                }
            }
        });

        this.setState({
            team_home: match.match.player1_name,
            team_guest: match.match.player2_name
        })
    }


    /**
     *
     * @param event
     */
    updateMail(event) {
        const type = event.target.name;
        const captainEmail = event.target.value;

        this.props.buildScoresheetStore({build: 'mail', data: {type, team: {captainEmail}}});
    }


    render() {
        if (!this.state.isLoaded) {
            return (
                <div className="loading-inline">
                    <div className="spinner -small">
                        <div className="rect1"></div>
                        <div className="rect2"></div>
                        <div className="rect3"></div>
                        <div className="rect4"></div>
                        <div className="rect5"></div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="container">

                    {this.renderCupMatches()}

                    <div className="row input-group">
                        <label htmlFor="team_home">Heimmannschaft</label>
                        <input id="team_home" type="text" className="input-text"
                               placeholder="-"
                               name="team_home"
                               disabled={true}
                               value={this.state.team_home}/>
                    </div>

                    <div className="row input-group">
                        <label htmlFor="team_guest">Gastmannschaft</label>
                        <input id="team_guest" type="text" className="input-text"
                               placeholder="-"
                               name="team_guest"
                               disabled={true}
                               value={this.state.team_guest}/>
                    </div>

                    <div className="row input-group">
                        <label htmlFor="mail_home">E-Mail Heimmannschaft</label>
                        <input id="mail_home" type="email" className="input-text"
                               placeholder="Heimkapitän"
                               name="team_home"
                               onChange={this.updateMail} required/>
                    </div>

                    <div className="row input-group">
                        <label htmlFor="mail_guest">E-Mail Gastmannschaft</label>
                        <input id="mail_guest" type="email" className="input-text"
                               placeholder="Gastkapitän"
                               name="team_guest"
                               onChange={this.updateMail} required/>
                    </div>

                </div>
            );
        }
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {showPageLoading, hidePageLoading, buildScoresheetStore})(FormSetupCup);
