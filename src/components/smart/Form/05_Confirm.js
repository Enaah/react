import React from "react";
import {connect} from "react-redux";
import {buildScoresheetStore} from "Redux/scoresheetStore";
import {hidePageLoading, showPageLoading} from "Redux/loadingStore";

import {MatchState} from "JS/enums/MatchState";
import Scoresheet from "JS/rest/Scoresheet";

import "./confirm-match.scss";
import Tracking from "JS/rest/Tracking";

class FormConfirm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.confirm = this.confirm.bind(this);
        this.revert = this.revert.bind(this);
    }

    componentDidMount() {
        this.setState({
            isLoaded: true
        });
    }


    /**
     *
     * @param event
     */
    revert(event) {
        event.preventDefault();
        this.props.buildScoresheetStore({
            build: 'state',
            data: MatchState.IN_PROGRESS
        });
    }

    /**
     *
     * @param event
     */
    confirm(event) {
        event.preventDefault();
        let {scoresheet} = this.props.scoresheetStore;

        Scoresheet.set.finalize(scoresheet).then(s => {
            Tracking.click('enter', MatchState.DONE);
            window.location.href = `#/live/${s.matchid}`;
        });
    }

    render() {
        const {scoresheet} = this.props.scoresheetStore;

        if (!this.state.isLoaded) {
            return null;
        } else {
            return (
                <div className="create-confirm-match">
                    <div className="section-head">
                        <div className="container">
                            <h1>Fast fertig</h1>
                            <p>Bitte überprüfe deine Eingaben und sende den Spielbogen dann final ab!</p>
                        </div>
                    </div>

                    <div className="section-form">
                        <div className="container">
                            <div className="row">
                                <div></div>
                                <div className="table -striped">
                                    <div className="table-row-confirm">
                                        <div className="align-right"><strong>Match ID</strong></div>
                                        <div>{scoresheet.matchid}</div>
                                    </div>
                                    <div className="table-row-confirm">
                                        <div className="align-right"><strong>Spielart</strong></div>
                                        <div>{scoresheet.gameType}</div>
                                    </div>

                                    <div className="table-row-confirm">
                                        <div className="align-right"><strong>Heimmannschaft</strong></div>
                                        <div>{scoresheet.team_home.name}</div>
                                    </div>
                                    <div className="table-row-confirm">
                                        <div className="align-right"><strong>Gastmannschaft</strong></div>
                                        <div>{scoresheet.team_guest.name}</div>
                                    </div>

                                    <div className="table-row-confirm">
                                        <div className="align-right"><strong>Torverhältnis</strong></div>
                                        <div className="score">
                                            <div className="score-home">
                                                {scoresheet.team_home.goals}:{scoresheet.team_guest.goals}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="table-row-confirm">
                                        <div className="align-right"><strong>Satzverhältnis</strong></div>
                                        <div className="score">
                                            <div className="score-home">
                                                {scoresheet.team_home.set}:{scoresheet.team_guest.set}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="section-submit">
                        <div className="container">
                            <div className="row">
                                <div></div>
                                <div data-grid="2">
                                    <button className="btn-default" data-icon="angle-left" onClick={this.revert}>
                                        zurück
                                    </button>
                                    <button className="btn-cta" data-icon="envelope" onClick={this.confirm}>
                                        Spielbogen bestätigen
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }

}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
    buildScoresheetStore
})(FormConfirm);
