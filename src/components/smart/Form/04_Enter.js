import React from "react";

import {showPageLoading, hidePageLoading} from "Redux/loadingStore";
import {showNotification} from "Redux/notificationStore";
import {setScoresheetStore, buildScoresheetStore, setPlayerPerformance} from "Redux/scoresheetStore";

import Player from "JS/utils/Player";
import {KKL} from "JS/setup/KKL";

import "./enter-result.scss";
import {connect} from "react-redux";
import Scoresheet from "JS/rest/Scoresheet";
import {MatchState} from "JS/enums/MatchState";
import {NotificationType} from "JS/enums/NotificationType";
import Tracking from "JS/rest/Tracking";

class FormEnter extends React.Component {

    constructor(props) {
        super(props);

        this.updateScore = this.updateScore.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        this.setState({submit: false});
    }


    /**
     *
     * @param event
     * @param match
     * @param set
     */
    updateScore(event, match, set) {
        this.props.buildScoresheetStore({
            build: 'score',
            data: {
                set,
                match,
                team: event.target.name,
                score: event.target.value
            }
        });
    }


    /**
     * toggle submit button
     */
    validateScoresheet() {
        const {scoresheet} = this.props.scoresheetStore;
        let submit = false;

        if ((scoresheet.team_home.set + scoresheet.team_guest.set) === (KKL.AMOUNT_MATCHES * KKL.AMOUNT_SETS)) {
            submit = true;
        }

        return submit;
    }


    /**
     * submit form
     * @param event
     */
    submitForm(event) {
        event.preventDefault();
        const {scoresheet} = this.props.scoresheetStore;
        Scoresheet.set.match(scoresheet).then(() => {
            this.props.buildScoresheetStore({build: 'state', data: MatchState.CONFIRM});
            Tracking.click('enter', MatchState.CONFIRM);
        });
    }


    /**
     * build html select
     * @param match
     * @param set
     * @param team
     * @param value
     * @returns {*}
     */
    htmlEnterScore(match, set, team, value) {
        const options = []
        for (let i = 0; i <= KKL.MAX_SCORE; i++) {
            options.push(<option value={i} key={`${match}_${set}_${team}_${value}_${i}`}>{i}</option>)
        }

        return (
            <select name={team} className="input-select"
                    onChange={(e) => this.updateScore(e, match, set)}
                    defaultValue={value}
                    key={`${match}_${set}_${team}_${value}`}>
                {options}
            </select>
        )
    }


    /**
     * save scoresheet in database initial & after changes
     */
    saveScoresheet() {
        Scoresheet.set.match(this.props.scoresheetStore.scoresheet)
            .then(() => this.props.showNotification({
                type: NotificationType.SUCCESS,
                message: 'Scoresheet saved in Database'
            }))
            .catch(() => this.props.showNotification({
                type: NotificationType.ERROR,
                message: 'Scoresheet cannot be saved'
            }));
    }


    render() {
        const {scoresheet} = this.props.scoresheetStore;

        if (!scoresheet.matchid) {
            return null;
        } else {
            this.saveScoresheet();

            return (
                <form className="enter-result" onSubmit={this.submitForm} noValidate>
                    <div className="table -tiles">
                        <div className="table-head-draw">
                            <div className="index"></div>
                            <div className="team-home">Heim</div>
                            <div className="divider"></div>
                            <div className="team-guest">Gast</div>
                            <div className="score">
                                <div className="score-home">Satz 1</div>
                                <div className="score-guest">Satz 2</div>
                            </div>
                        </div>

                        {scoresheet.matches.map((r, index) => (
                            <div className="table-row-draw" key={index}>
                                <div className="index">{index + 1}.</div>
                                <div className="team-home">
                                    {Player.shortName(r.player.home_a)}<br/>
                                    {Player.shortName(r.player.home_b)}
                                </div>
                                <div className="divider">vs</div>
                                <div className="team-guest">
                                    {Player.shortName(r.player.guest_a)}<br/>
                                    {Player.shortName(r.player.guest_b)}
                                </div>
                                <div className="score">
                                    <div className="score-home">
                                        {this.htmlEnterScore(index, 0, 'home', r.score[0].home)}
                                        <span>:</span>
                                        {this.htmlEnterScore(index, 0, 'guest', r.score[0].guest)}
                                    </div>
                                    <div className="score-guest">
                                        {this.htmlEnterScore(index, 1, 'home', r.score[1].home)}
                                        <span>:</span>
                                        {this.htmlEnterScore(index, 1, 'guest', r.score[1].guest)}
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>

                    <div className="submit">
                        <div></div>
                        <button type="submit"
                                className="btn-cta"
                                data-icon="submit"
                                disabled={!this.validateScoresheet()}>
                            Begegnung abschließen
                        </button>
                    </div>

                </form>
            );
        }
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
    showNotification,
    setScoresheetStore,
    buildScoresheetStore,
    setPlayerPerformance
})(FormEnter);
