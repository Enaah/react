import React from "react";
import {connect} from "react-redux";
import Kickerliga from "JS/rest/Kickerliga";

class LiveTable extends React.Component {

    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount() {
        const currentSeason = this.props.season || null;

        if (currentSeason) {
            Kickerliga.ranks(currentSeason).then((ranks) => {
                this.setState({ranks});
            });
        }
    }


    render() {
        const {ranks} = this.state;

        if (!ranks) {
            return null;
        } else {
            return (
                <div className="live-table">
                    <div className="table -tiles">
                        <div className="table-head-ranks">
                            <div className="index">#</div>
                            <div className="team">Team</div>
                            <div className="games">Spiele</div>
                            <div className="rate">g/u/v</div>
                            <div className="diff">Diff</div>
                            <div className="score">Punkte</div>
                        </div>

                        {ranks.map((rank, index) => (
                            <div className="table-row-ranks" key={rank.teamId}>
                                <div className="index">{index + 1}.</div>
                                <div className="team"><strong>{rank.team.name}</strong></div>
                                <div className="games">{rank.games}</div>
                                <div className="rate">{rank.wins}/{rank.draws}/{rank.losses}</div>
                                <div className="diff">{rank.gameDiff}</div>
                                <div className="score">{rank.score}</div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = state => ({
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {})(LiveTable);
