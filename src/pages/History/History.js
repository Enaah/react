import React, {useEffect, useState} from 'react';
import {connect, useDispatch} from 'react-redux'
import {showPageLoading, hidePageLoading} from "Redux/loadingStore";

import moment from "moment";
import {Collapse} from "react-collapse/lib/Collapse";

import {GameType} from "JS/enums/GameType";
import {Helmet} from "react-helmet";
import Scoresheet from "JS/rest/Scoresheet";
import Tracking from 'JS/rest/Tracking';
import {Topbar} from "Dumb/Topbar/Topbar";
import {Footerbar} from "Dumb/Footerbar/Footerbar";
import {Match} from "Dumb/Match/Match";
import ToggleGameType from "Smart/Form/ToggleGameType";

import './history.scss'


const History = (props) => {
    const dispatch = useDispatch();
    const [fetchComplete, setFetchComplete] = useState(false);
    const [collapseOpen, setCollapseOpen] = useState(null);

    const currentSeason = new Date().getFullYear();
    const firstSeason = 2017;
    let allSeasons = [];
    for (let i = currentSeason; i >= firstSeason; i--) {
        allSeasons.push(i);
    }
    const [activeSeason, setActiveSeason] = useState(currentSeason);

    const [currentGameType, setGameType] = useState(GameType.GAMEDAY)
    const [matchesGameday, setMatchesGameday] = useState([]);
    const [matchesCup, setMatchesCup] = useState([]);
    const [matchesCustom, setMatchesCustom] = useState([]);

    // initial call
    useEffect(() => {
        fetchSeasonData(activeSeason);
    }, [])


    /**
     *
     * @param season
     */
    function fetchSeasonData(season) {
        dispatch(showPageLoading('Lade Spiele'));
        Tracking.click('history', season);

        let promises = [
            Scoresheet.live.all('gameday', season),
            Scoresheet.live.all('custom', season),
            Scoresheet.live.all('cup', season),
        ];

        Promise.all(promises).then(result => {
            const [gameday, custom, cup] = result;

            setCollapseOpen(null);
            setFetchComplete(true);

            setActiveSeason(season);
            setMatchesGameday(gameday);
            setMatchesCup(cup);
            setMatchesCustom(custom);

            dispatch(hidePageLoading());
        });
    }


    /**
     *
     * @param id
     */
    function triggerCollapse(id) {
        if (collapseOpen === id) {
            setCollapseOpen(null);
        } else {
            setCollapseOpen(id);
        }
    }


    /**
     *
     * @returns {*}
     */
    function htmlGameday() {
        if (matchesGameday.length > 0) {
            return (
                <div>
                    <h1>Ligaspiele</h1>
                    {matchesGameday.map((gameday, index) => (
                        <div key={`gameday_${gameday._id}`} className="gameday">
                            <button
                                className={`collapseToggle ${('gameday_' + gameday._id === collapseOpen) ? '-active' : ''}`}
                                id={`gameday_${gameday._id}`}
                                onClick={({target: {id}}) => triggerCollapse(id)}>
                                {gameday._id}. Spieltag
                                <span className="pill -small">{gameday.sum}</span>
                            </button>

                            <Collapse isOpened={(`gameday_${gameday._id}` === collapseOpen)}>
                                <Match matches={gameday.data}/>
                            </Collapse>
                        </div>
                    ))}
                </div>
            );
        } else {
            return (
                <div>
                    <p>No matches found.</p>
                </div>
            );
        }
    }


    /**
     *
     * @returns {JSX.Element}
     */
    function htmlCup() {
        if (matchesCup.length > 0) {
            return (
                <div>
                    <h1>Pokalspiele</h1>
                    {matchesCup.map((month, index) => (
                        <div key={`cup_${month._id}`} className="cup">
                            <button
                                className={`collapseToggle ${('cup_' + month._id === collapseOpen) ? '-active' : ''}`}
                                id={`cup_${month._id}`}
                                onClick={({target: {id}}) => triggerCollapse(id)}>
                                {moment(month._id).format('MMMM')}
                                <span className="pill -small">{month.sum}</span>
                            </button>

                            <Collapse isOpened={(`cup_${month._id}` === collapseOpen)}>
                                <Match matches={month.data}/>
                            </Collapse>
                        </div>
                    ))}
                </div>
            );
        } else {
            return (
                <div>
                    <p>No matches found.</p>
                </div>
            );
        }
    }


    /**
     *
     * @returns {*}
     */
    function htmlOther() {
        if (matchesCustom.length > 0) {
            return (
                <div>
                    <h1>Weitere Spiele</h1>
                    {matchesCustom.map((month, index) => (
                        <div key={`custom_${month._id}`} className="custom">
                            <button
                                className={`collapseToggle ${('custom_' + month._id === collapseOpen) ? '-active' : ''}`}
                                id={`custom_${month._id}`}
                                onClick={({target: {id}}) => triggerCollapse(id)}>
                                {moment(month._id).format('MMMM')}
                                <span className="pill -small">{month.sum}</span>
                            </button>

                            <Collapse isOpened={(`custom_${month._id}` === collapseOpen)}>
                                <Match matches={month.data}/>
                            </Collapse>
                        </div>
                    ))}
                </div>
            );
        } else {
            return (
                <div>
                    <p>No matches found.</p>
                </div>
            );
        }
    }


    return (
        <div id="live">
            <Helmet>
                <title>{`Begegnungen | Kölner Kickerliga`}</title>
            </Helmet>
            <Topbar/>

            <section className="live-nav-season">
                <div className="container">
                    <ul>
                        {allSeasons.map(season => (
                            <li key={season}>
                                <button className={(activeSeason === season ? '-active' : '')}
                                        onClick={(() => fetchSeasonData(season))}>{season}</button>
                            </li>
                        ))}
                    </ul>
                </div>
            </section>

            <section className="history-gametype">
                <div className="container">
                    <ToggleGameType
                        types={[GameType.GAMEDAY, GameType.CUP, GameType.CUSTOM]}
                        emitEvent={t => setGameType(t)}
                    />
                </div>
            </section>

            <section className="history-matches">
                <div className="container">
                    {(currentGameType === GameType.GAMEDAY) ? htmlGameday() : null}
                    {(currentGameType === GameType.CUP) ? htmlCup() : null}
                    {(currentGameType === GameType.CUSTOM) ? htmlOther() : null}
                </div>
            </section>

            <div className="-spacer"></div>
            <Footerbar/>
        </div>
    );
}

export default History;
