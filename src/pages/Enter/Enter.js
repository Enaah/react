import React from "react";
import {connect} from "react-redux";
import {showPageLoading, hidePageLoading} from "Redux/loadingStore";
import {setScoresheetStore, buildScoresheetStore, setPlayerPerformance} from "Redux/scoresheetStore";

import Store from "JS/utils/Store";
import {GameType} from "JS/enums/GameType";
import {Helmet} from "react-helmet";
import Slider from "react-slick";
import moment from "moment";

import {Topbar} from "Dumb/Topbar/Topbar";
import {Footerbar} from "Dumb/Footerbar/Footerbar";
import {ScoresheetTitle} from "Dumb/Title/Title";
import {ScoresheetTrend} from "Dumb/Trend/Trend";
import {ScoresheetGamedata} from "Dumb/Gamedata/Gamedata";
import {ScoresheetPlayers} from "Dumb/Players/Players";
import {ScoresheetShare} from "Dumb/Share/Share";
import {ScoresheetMap} from "Dumb/Map/Map";
import FormEnter from "Smart/Form/04_Enter";
import LiveTable from "Smart/LiveTable/LiveTable";
import LiveGames from "Smart/LiveGames/LiveGames";
import {MatchState} from "JS/enums/MatchState";
import FormSetup from "Smart/Form/01_Setup";
import FormPlayers from "Smart/Form/02_Players";
import FormDraw from "Smart/Form/03_Draw";
import FormConfirm from "Smart/Form/05_Confirm";

import "./enter.scss";
import Performance from "JS/rest/Performance";

class Enter extends React.Component {
    static defaultProps = {
        pagination: ['Spielbericht', 'Spielinfo', 'Tabelle']
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const scoresheet = Store.getLocalStorage('scoresheet') || null;
        if (scoresheet) {
            this.props.setScoresheetStore(scoresheet);

            Performance.players(scoresheet.matchid)
                .then(performance => this.props.setPlayerPerformance(performance));
        }
    }


    htmlEnterResults() {
        const {scoresheet} = this.props.scoresheetStore;
        const settings = {
            dots: true,
            autoplay: false,
            arrows: false,
            infinite: false,
            speed: 330,
            slidesToShow: 1,
            slidesToScroll: 1,
            customPaging: (i) => {
                return (
                    <span>{this.props.pagination[i]}</span>
                );
            },
        };

        return (
            <div>
                <ScoresheetTitle/>

                <Slider {...settings}>
                    <div data-title="Spielbericht">
                        <section className="container">
                            <div className="section-head">
                                <h1>Ergebnisse</h1>
                            </div>
                            <FormEnter/>
                        </section>

                        <section className="container">
                            <div className="section-head">
                                <h1>Spielverlauf</h1>
                            </div>

                            <ScoresheetTrend/>
                        </section>
                    </div>

                    <div data-title="Spieldaten">
                        <section className="container">
                            <div className="section-head">
                                {(scoresheet.gameType === GameType.GAMEDAY) ? (
                                    <h1>Ligaspiel</h1>
                                ) : null}

                                {(scoresheet.gameType === GameType.CUP) ? (
                                    <h1>Pokalspiel</h1>
                                ) : null}

                                {(scoresheet.gameType === GameType.CUSTOM) ? (
                                    <h1>Freundschaftsspiel</h1>
                                ) : null}
                            </div>
                            <ScoresheetGamedata/>
                        </section>

                        <hr/>
                        <section className="container">
                            <div data-grid="2">
                                <ScoresheetPlayers team="home"/>
                                <ScoresheetPlayers team="guest"/>
                            </div>
                        </section>
                    </div>

                    {(scoresheet && scoresheet.gameType === GameType.GAMEDAY) ? (
                        <div data-title="Tabelle">
                            <section className="container">
                                <div className="section-head">
                                    <h1>{scoresheet.matchInfo.leagueName}</h1>
                                </div>
                                <LiveTable season={scoresheet.matchInfo.currentSeason}/>
                            </section>

                            <section className="container">
                                <div className="section-head">
                                    <h1>Laufender Spieltag</h1>
                                </div>
                                <LiveGames season={moment(scoresheet.date.create).format('YYYY')}
                                           gameday={scoresheet.matchInfo.gamedayNumber}
                                           gameType={GameType.GAMEDAY}
                                           league={scoresheet.matchInfo.leagueCode}/>
                            </section>
                        </div>
                    ) : ''}
                </Slider>

                <ScoresheetMap/>
                <ScoresheetShare/>
            </div>
        )
    }


    render() {
        const {scoresheet} = this.props.scoresheetStore;
        console.log('store', scoresheet);

        return (
            <div id="create">
                {(scoresheet.state === MatchState.IN_PROGRESS) ? (
                    <Helmet>
                        <title>{`${scoresheet.team_home.name} vs ${scoresheet.team_guest.name} | Kölner Kickerliga`}</title>
                        <meta property="og:url"
                              content={`${window.location.origin}/live/${scoresheet.matchid}`}/>
                        <meta property="og:description"
                              content={`${scoresheet.team_home.name} vs ${scoresheet.team_guest.name} (${scoresheet.team_home.set}:${scoresheet.team_guest.set})`}/>
                    </Helmet>
                ) : null}

                <Topbar/>

                <div className="detail">
                    {(!scoresheet.state || scoresheet.state === MatchState.SETUP) ? <FormSetup/> : null}
                    {(scoresheet.state === MatchState.SETUP_PLAYERS) ? <FormPlayers/> : null}
                    {(scoresheet.state === MatchState.DRAW_MATCHES) ? <FormDraw/> : null}
                    {(scoresheet.state === MatchState.IN_PROGRESS) ? this.htmlEnterResults() : null}
                    {(scoresheet.state === MatchState.CONFIRM) ? <FormConfirm/> : null}
                </div>

                <div className="-spacer"></div>
                <Footerbar/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
    scoresheetStore: state.scoresheetStore
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading,
    setScoresheetStore,
    buildScoresheetStore,
    setPlayerPerformance
})(Enter);
