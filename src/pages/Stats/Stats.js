import React from "react";
import { connect } from "react-redux";
import { showPageLoading, hidePageLoading } from "Redux/loadingStore";
import { GameType } from "JS/enums/GameType";

import ToggleGameType from "Smart/Form/ToggleGameType";
import StatsTotals from "Smart/Stats/Totals";
import StatsDrawingType from "Smart/Stats/DrawingType";
import StatsFavoriteWeekDay from "Smart/Stats/FavoriteWeekDay";
import StatsTeamPerformance from "Smart/Stats/TeamPerformance";
import StatsDistributionResults from "Smart/Stats/DistributionResults";
import { Footerbar } from "Dumb/Footerbar/Footerbar";
import { Topbar } from "Dumb/Topbar/Topbar";

import "./stats.scss";


class Stats extends React.Component {

    constructor(props) {
        super(props);

        const currentSeason = new Date().getFullYear();
        const firstSeason = 2017;
        let allSeasons = [];
        for (let i = currentSeason; i >= firstSeason; i--) {
            allSeasons.push(i);
        }

        this.state = {
            activeSeason: currentSeason,
            activeGameType: GameType.GAMEDAY,
            allSeasons,
            allGameTypes: [GameType.GAMEDAY, GameType.CUP, GameType.CUSTOM]
        }
    }


    /**
     * toggle gametype and fetch stats
     * @param type
     */
    updateGameType(type) {
        this.setState({ activeGameType: type });
    }


    /**
     * toggle season and fetch stats
     * @param season
     */
    updateSeason(season) {
        this.setState({ activeSeason: season });
    }


    render() {
        const {
            allSeasons,
            activeSeason,
            allGameTypes,
            activeGameType
        } = this.state;

        return (
            <div id="statistics">
                <Topbar />
                <section className="live-nav-season">
                    <div className="container">
                        <ul>
                            {allSeasons.map(season => (
                                <li key={season}>
                                    <button className={(activeSeason === season ? '-active' : '')}
                                        onClick={(() => this.updateSeason(season))}>{season}</button>
                                </li>
                            ))}
                        </ul>
                    </div>
                </section>

                <section className="stats-gametype">
                    <div className="container">
                        <ToggleGameType
                            emitEvent={t => this.updateGameType(t)}
                            types={allGameTypes}
                        />
                    </div>
                </section>

                <StatsTotals season={activeSeason} gametype={activeGameType} />
                
                <section className="stats-charts">
                    <div className="container" data-grid="3">
                        <StatsDrawingType season={activeSeason} gametype={activeGameType} />
                        <StatsFavoriteWeekDay season={activeSeason} gametype={activeGameType} />
                        <StatsDistributionResults season={activeSeason} gametype={activeGameType} />
                    </div>
                </section>

                <section className="stats-teams">
                    <div className="container">
                        <StatsTeamPerformance season={activeSeason} gametype={activeGameType} />
                    </div>
                </section>

                <div className="-spacer"></div>
                <Footerbar />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    loadingStore: state.loadingStore,
});

export default connect(mapStateToProps, {
    showPageLoading,
    hidePageLoading
})(Stats);