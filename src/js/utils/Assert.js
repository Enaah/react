export default class Assert {

    MAX_SAFE_INTEGER = Number.MAX_SAFE_INTEGER || 9007199254740991;

    UNDEFINED_TYPE = "undefined";
    OBJECT_TYPE = "object";
    NUMBER_TYPE = "number";
    STRING_TYPE = "string";
    FUNCTION_TYPE = "function";
    BOOLEAN_TYPE = "boolean";
    SYMBOL_TYPE = "symbol";

    /**
     * Returns the size of the object
     *
     * @param obj
     * @param deep
     * @returns {number}
     */
    getObjectSize(obj, deep = false) {
        let size = 0;
        let key;

        for (key in obj) {
            if (!obj.hasOwnProperty(key)) {
                continue;
            }

            if (deep && this.isObject(obj[key])) {
                size += this.getObjectSize(obj[key], true) + 1;
            } else {
                size++;
            }
        }

        return size;
    }

    /**
     * Checks if the given object is undefined, null or empty
     *
     * @param obj Object to check
     * @returns {boolean}
     */
    isUndefinedNullOrEmpty(obj) {
        return this.isUndefined(obj) || this.isNull(obj) || this.isEmpty(obj);
    }

    /**
     * Checks if the given object is _NOT_ undefined, null or empty
     *
     * @param obj
     * @returns {boolean}
     */
    isNotUndefinedNullOrEmpty(obj) {
        return !this.isUndefinedNullOrEmpty(obj);
    }

    /**
     * Checks if the given object is not null
     *
     * @param obj
     * @returns {boolean}
     */
    nonNull(obj) {
        return !this.isNull(obj);
    }

    /**
     * Checks if the given object is empty
     *
     * @param obj
     * @returns {boolean}
     */
    isEmpty(obj) {
        if (this.isNull(obj)) {
            return true;
        }

        if (this.isOfType(obj, this.OBJECT_TYPE) && !this.isArray(obj)) {
            return this.getObjectSize(obj) <= 0;
        } else if (this.isOfType(obj, this.NUMBER_TYPE)) {
            return obj <= 0;
        } else if (this.isOfType(obj, this.FUNCTION_TYPE)) {
            return false;
        }

        return obj.length <= 0;
    }

    /**
     * Checks if the given object is not empty
     *
     * @param obj
     * @returns {boolean}
     */
    isNotEmpty(obj) {
        return !this.isEmpty(obj);
    }

    /**
     * Checks if the given object has a function with the given name
     *
     * @param obj
     * @param functionName
     * @returns {boolean}
     */
    hasFunction(obj, functionName) {
        return this.nonNull(obj) && this.isOfType(obj[functionName], this.FUNCTION_TYPE);
    }

    /**
     * Checks if the given object is of the expected type
     *
     * @param obj
     * @param expectedType
     * @returns {boolean}
     */
    isOfType(obj, expectedType) {
        return typeof obj === expectedType;
    }

    /**
     * Checks if the given object is a instance of the given type
     *
     * @param obj
     * @param type
     * @returns {boolean}
     */
    isInstanceOf(obj, type) {
        return obj instanceof type;
    }

    /**
     * Checks if the given object is a safe integer
     *
     * From https://mdn.io/Number/isSafeInteger
     *
     * @param obj
     * @returns {boolean}
     */
    isSafeInteger(obj) {
        if (!this.isNumber(obj) || this.isFloat(obj)) {
            return false;
        }

        return obj >= -this.MAX_SAFE_INTEGER && obj <= this.MAX_SAFE_INTEGER
    }

    /**
     * Checks if the given property exists in the object
     *
     * @param obj
     * @param property
     * @returns {boolean}
     */
    hasProperty(obj, property) {
        if (this.isUndefinedNullOrEmpty(obj)) {
            return false;
        }

        if (this.isArray(obj)) {
            return false;
        }

        return obj.hasOwnProperty(property);
    }

    // -- Primitive checks

    /**
     * Checks if the given object is undefined
     *
     * @param obj
     * @returns {boolean}
     */
    isUndefined(obj) {
        return this.isOfType(obj, this.UNDEFINED_TYPE);
    }

    /**
     * Checks if the given object is null
     *
     * @param obj
     * @returns {boolean}
     */
    isNull(obj) {
        if (this.isUndefined(obj)) {
            return true;
        }

        return obj === null;
    }

    /**
     * Checks if the given object is array-like
     *
     * @param obj
     * @returns {boolean}
     */
    isArrayLike(obj) {
        return this.nonNull(obj) && this.hasLength(obj) && !this.isFunction(obj);
    }

    /**
     * Checks if the given object is a array
     *
     * @param obj
     * @returns {boolean}
     */
    isArray(obj) {
        return Array.isArray(obj);
    }

    /**
     * Checks if the given object is a function
     *
     * @param obj
     * @returns {boolean}
     */
    isFunction(obj) {
        return this.nonNull(obj) && this.isOfType(obj, this.FUNCTION_TYPE);
    }

    /**
     * Checks if the given object has a length
     *
     * @param obj
     * @returns {boolean}
     */
    hasLength(obj) {
        return this.nonNull(obj.length);
    }

    /**
     * Checks if the given object is a object
     *
     * @param obj
     * @returns {boolean}
     */
    isObject(obj) {
        return this.isOfType(obj, this.OBJECT_TYPE) && !this.isArray(obj) && this.hasFunction(obj, "hasOwnProperty");
    }

    /**
     * Checks if the given object is a boolean
     *
     * @param obj
     * @returns {boolean}
     */
    isBoolean(obj) {
        return this.isOfType(obj, this.BOOLEAN_TYPE);
    }

    /**
     * Checks if the given object is a number
     *
     * @param obj
     * @returns {boolean}
     */
    isNumber(obj) {
        return this.isOfType(obj, this.NUMBER_TYPE);
    }

    /**
     * Checks if the given object is maybe a number
     *
     * @param obj
     * @returns {boolean}
     */
    isMaybeNumber(obj) {
        return !isNaN(obj);
    }

    /**
     * Checks if the given object is a float
     *
     * @param obj
     * @returns {boolean}
     */
    isFloat(obj) {
        if (this.isNull(obj)) {
            return false;
        }

        return obj % 1 > 0;
    }

    /**
     * Checks if the given object is symbol
     *
     * @param obj
     * @returns {boolean}
     */
    isSymbol(obj) {
        return this.isOfType(obj, this.SYMBOL_TYPE);
    }
}