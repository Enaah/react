import {TeamType} from "../enums/TeamType";
import {KKL} from "../setup/KKL";

export default class Calculate {

    /**
     *
     * @param scoresheet
     * @param match
     * @param team
     * @param set
     * @returns {{set: *, match: *, team: *}}
     */
    static winner(scoresheet, match, team, set) {
        let winner = null;

        if (scoresheet[team].set === 11 && !scoresheet['winner']) {
            winner = {
                match,
                team,
                set
            };
        }

        return winner;
    }


    /**
     *
     * @param scoresheet
     * @returns {*}
     */
    static score(scoresheet) {
        scoresheet[TeamType.HOME].goals = 0;
        scoresheet[TeamType.HOME].set = 0;
        scoresheet[TeamType.GUEST].goals = 0;
        scoresheet[TeamType.GUEST].set = 0;

        for (let i = 0; i < scoresheet.matches.length; i++) {       // [0 ... 9]
            for (let set in scoresheet.matches[i].score) {          // [{home, guest}, {home, guest}]

                for (let key in scoresheet.matches[i].score[set]) { // {home, guest}
                    let value = parseInt(scoresheet.matches[i].score[set][key]) || 0;

                    switch (key) {
                        case 'home':
                            let win = false;

                            scoresheet[TeamType.HOME]['goals'] += value;
                            if (value === KKL.MAX_SCORE) {
                                scoresheet[TeamType.HOME]['set']++;
                                win = true;
                            }

                            scoresheet['winner'] = this.winner(
                                scoresheet,
                                i + 1,
                                TeamType.HOME,
                                parseFloat(key.split('_')[1])
                            );

                            break;

                        case 'guest':
                            scoresheet[TeamType.GUEST]['goals'] += value;
                            if (value === KKL.MAX_SCORE) {
                                scoresheet[TeamType.GUEST]['set']++;
                            }

                            scoresheet['winner'] = this.winner(
                                scoresheet,
                                i + 1,
                                TeamType.GUEST,
                                parseFloat(key.split('_')[1])
                            );

                            break;

                        default:
                    }
                }
            }
        }

        return scoresheet;
    };

}