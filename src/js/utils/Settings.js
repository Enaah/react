export const Settings = {


    /**
     *
     * @param key
     * @returns {string | null}
     */
    get: (key) => {
        return process.env[key] || null;
    }
};
