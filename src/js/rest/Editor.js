import axios from "axios";
import Store from "../utils/Store";

export default class Editor {

    static set(matchid) {
        return new Promise(resolve => {
            axios.post(`/api/v2/editor/${matchid}`, {})
                .then(resp => {
                    const today = new Date();
                    const tomorrow = new Date()

                    let expireDateString = tomorrow.setDate(today.getDate() + 2);

                    Store.setCookie('EDITOR_AUTH', resp['data']['result'].token, expireDateString);
                    resolve(resp['data']['result'].token);
                });
        })
    }

}
