import {createSlice} from "@reduxjs/toolkit";
import Factory from "JS/utils/Factory";
import {MatchState} from "JS/enums/MatchState";
import Store from "../js/utils/Store";
import Draw from "../js/utils/Draw";

export const scoresheetStore = createSlice({
    name: "scoresheet",
    initialState: {
        scoresheet: {},
        playerPerformance: []
    },
    reducers: {
        revertScoresheetStore: (state) => {
            Store.removeLocalStorage('scoresheet');
            state.scoresheet = {};
        },
        setScoresheetStore: (state, action) => {
            state.scoresheet = action.payload;
        },
        setPlayerPerformance: (state, action) => {
            state.playerPerformance = action.payload;
        },

        buildScoresheetStore: (state, action) => {
            const {build, data} = action.payload;

            state.scoresheet = Factory.set.date(state.scoresheet);

            // TODO: simplify!?
            switch (build) {
                case 'state':
                    state.scoresheet = Factory.set.state(state.scoresheet, data);
                    break;
                case 'id':
                    state.scoresheet = Factory.set.state(state.scoresheet, MatchState.SETUP);
                    state.scoresheet = Factory.set.id(state.scoresheet, data);
                    break;
                case 'user':
                    state.scoresheet = Factory.set.user(state.scoresheet, data);
                    break;
                case 'match':
                    state.scoresheet = Factory.set.user(state.scoresheet);
                    state.scoresheet = Factory.set.match(state.scoresheet, data);
                    break;
                case 'location':
                    state.scoresheet = Factory.set.location(state.scoresheet, data);
                    break;
                case 'team':
                    state.scoresheet = Factory.set.team(state.scoresheet, data);
                    break;
                case 'mail':
                    state.scoresheet = Factory.set.mail(state.scoresheet, data);
                    break;
                case 'player':
                    state.scoresheet = Factory.set.player(state.scoresheet, data);
                    break;
                case 'draw':
                    if(data) {
                        state.scoresheet = Draw.custom.set(state.scoresheet, data);
                    } else {
                        state.scoresheet = Draw.kkl(state.scoresheet);
                    }
                    break;
                case 'score':
                    state.scoresheet = Factory.set.score(state.scoresheet, data);
                    break;
            }

            Store.setLocalStorage('scoresheet', state.scoresheet);
        }
    }
});

export const {
    revertScoresheetStore,
    setScoresheetStore,
    setPlayerPerformance,
    buildScoresheetStore
} = scoresheetStore.actions;

export default scoresheetStore.reducer;
