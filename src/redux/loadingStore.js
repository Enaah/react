import { createSlice } from "@reduxjs/toolkit";

export const loadingStore = createSlice({
    name: 'loading',
    initialState: {
        pending: false,
        message: null
    },
    reducers: {
        showPageLoading: (state, action) => {
            state.pending = true;
            state.message = action.payload || null;
        },
        hidePageLoading: (state, action) => {
            state.pending = false;
            state.message = null;
        }
    }
});

export const {
    showPageLoading,
    hidePageLoading
} = loadingStore.actions;

export default loadingStore.reducer;
