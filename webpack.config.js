const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const dotenv = require('dotenv');
const webpack = require('webpack');

module.exports = (env) => {
    // use .env file if is available
    // otherwise define env-vars during the build
    // npm run build --env AUTH0_DOMAIN=xyz
    const dotenvFile = dotenv.config().parsed;
    if (dotenvFile) {
        env = dotenvFile
    }

    const envKeys = Object.keys(env).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(env[next]);
        return prev;
    }, {});

    return {
        plugins: [
            new HtmlWebpackPlugin({
                template: path.join(__dirname, "src", "index.html"),
            }),
            new webpack.DefinePlugin(envKeys)
        ],

        mode: 'production',
        devServer: {
            port: 3000,
            proxy: {
                '/api': {
                    target: 'http://localhost:8888'
                },
            }
        },
        entry: path.join(__dirname, "src", "index.js"),
        output: {
            path: path.resolve(__dirname, "dist"),
        },
        resolve: {
            alias: {
                CssWorkgroup: path.resolve(__dirname, 'src/css/_workgroup.scss'),
                Images: path.resolve(__dirname, 'src/images/'),
                Fonts: path.resolve(__dirname, 'src/fonts/'),
                JS: path.resolve(__dirname, 'src/js/'),
                Pages: path.resolve(__dirname, 'src/pages/'),
                Smart: path.resolve(__dirname, 'src/components/smart/'),
                Dumb: path.resolve(__dirname, 'src/components/dumb/'),
                Redux: path.resolve(__dirname, 'src/redux/')
            },
            extensions: [
                '.tsx',
                '.ts',
                '.js',
                '.scss'
            ]
        },
        module: {
            rules: [
                {
                    test: /\.?js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-react']
                        }
                    }
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        "style-loader",
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: {
                                // Prefer `dart-sass`
                                implementation: require.resolve("sass"),
                            },
                        },
                    ],
                },
                {
                    test: /\.svg$/i,
                    issuer: /\.[jt]sx?$/,
                    use: ['@svgr/webpack'],
                },
                {
                    test: /\.(ttf|eot|woff|woff2)$/,
                    type: 'asset/resource',
                    include: [
                        path.resolve(__dirname, "src/fonts")
                    ]
                },
                {
                    test: /\.(png|jp(e*)g|gif)$/,
                    type: 'asset/resource',
                    include: [
                        path.resolve(__dirname, "src/images")
                    ],
                }
            ]
        }
    }
};
